import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from "@angular/forms";
import { AutentificacionRoutingModule } from './autentificacion-routing.module';
import { AutentificacionComponent } from './autentificacion.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { AutentificacionService } from './autentificacion.service';
import { AutentificacionStaticService } from './autentificacion-static.service';
import { LoginGuard } from './login.guard';

@NgModule({
  imports: [
    CommonModule,
    AutentificacionRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [AutentificacionComponent, LoginComponent, LogoutComponent],
  providers: [AutentificacionService, AutentificacionStaticService, LoginGuard],
  exports:[LogoutComponent]
})
export class AutentificacionModule { }
