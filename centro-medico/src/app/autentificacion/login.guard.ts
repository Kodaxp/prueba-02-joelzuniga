import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AutentificacionStaticService } from './autentificacion-static.service';

@Injectable()
export class LoginGuard implements CanActivate {
  canActivate(): boolean{
    if (null != AutentificacionStaticService.buscarUsuarioEnSesion()){
      console.log("Posee sesion o permisos");
      return true;
    }
    console.log("No posee sesion activa");
    return false;
  }
}
