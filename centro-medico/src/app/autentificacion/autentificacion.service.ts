import { Injectable } from '@angular/core';
import { Usuario } from './usuario';
import { HttpParams, HttpClient } from '@angular/common/http';

@Injectable()
export class AutentificacionService {

  public URL_API = "http://localhost:3004/usuarios";

  constructor(private httpClient:HttpClient) { }

  login(usaurio:Usuario){
    let httpParams = new HttpParams();
    httpParams = httpParams.set("contrasena", usaurio.contrasena.toString());
    httpParams = httpParams.set("usuario", usaurio.usuario.toString());
    console.log(httpParams.toString());
    return this.httpClient.get<Usuario[]>(this.URL_API, {params:httpParams})
  }

}
