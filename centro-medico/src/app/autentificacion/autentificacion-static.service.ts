import { Injectable } from '@angular/core';
import { Usuario } from './usuario';

const NOMBRE_USUARIO = "medico";

@Injectable()
export class AutentificacionStaticService {

  constructor() { }
//GUARDAR USUARIO EN LOCAL STORAGE

static guardarEnSesion(nombre:string, usuario:string){
  localStorage.setItem(nombre, usuario);
}

static guardarUsuarioEnSesion(usuario:Usuario){
  return this.guardarEnSesion(NOMBRE_USUARIO, JSON.stringify(usuario));
}

//CERRAR SESION

static cerrarSesion(){
  localStorage.clear();
}

//VALIDACION CON GUARD

static buscarEnSesion(nombre:string):string{
  return localStorage.getItem(nombre);
}

static buscarUsuarioEnSesion(): Usuario{
  if (null == this.buscarEnSesion(NOMBRE_USUARIO)) {
    return null;
  }
  let usuario:Usuario;
  usuario = JSON.parse(this.buscarEnSesion(NOMBRE_USUARIO));
  return usuario;
}
}
