import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AutentificacionService } from '../autentificacion.service';
import { Router } from '@angular/router';
import { Usuario } from '../usuario';
import { AutentificacionStaticService } from '../autentificacion-static.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public formularioLogin:FormGroup;

  public usuario: FormControl;
  public contrasena: FormControl;

  constructor(private servicio:AutentificacionService, private router:Router) { 
    this.usuario = new FormControl('', Validators.required);
    this.contrasena = new FormControl('', Validators.required);
  }

  ngOnInit() {
    this.formularioLogin = new FormGroup({
      usuario: this.usuario,
      contrasena: this.contrasena
    });
  }

  loginUsuario(){
    if (this.formularioLogin.valid) {
      let usuario = new Usuario();
      usuario.usuario = this.formularioLogin.value.usuario;
      usuario.contrasena = this.formularioLogin.value.contrasena;
      console.log(usuario);
      this.servicio.login(usuario).subscribe(
        (respuesta) => {
          // usuario = respuesta;
          if (respuesta.length > 0) {
            usuario = respuesta[0];
            console.log(usuario);
            if (usuario != null) {
              usuario.contrasena = '';
              AutentificacionStaticService.guardarUsuarioEnSesion(usuario);
              this.router.navigate(['ficha-paciente/crear']);
            }
          }
          
          // if (usuario != null) {
          //   if (usuario.usuario != "" || usuario.contrasena != "") {
          //     AutentificacionStaticService.guardarUsuarioEnSesion(usuario);
          //     this.router.navigate(['ficha-paciente/crear']);
          //   }
          // }
        }
      );
    }
  }

}
