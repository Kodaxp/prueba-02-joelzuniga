import { Component, OnInit } from '@angular/core';
import { AutentificacionStaticService } from '../autentificacion-static.service';
import { Router } from '@angular/router';
import { Usuario } from '../usuario';
import { FichaPaciente } from '../../ficha-paciente/ficha-paciente';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  public val:Usuario;
  public listadoPacientes:FichaPaciente[];
  public nombreMedico:Usuario;
  public nombre:String;
  public apellido:String;
  public estado:String;

  constructor(private router:Router) { 
    this.estado = "Ofline";
    this.nombre = "";

  }

  ngOnInit() {
    this.nombreMedico = AutentificacionStaticService.buscarUsuarioEnSesion();
    this.nombre = this.nombreMedico.nombre;
    this.estado = "Online :";
  }

  cerrarSesion(){
    console.log("Sesion Cerrada");
    AutentificacionStaticService.cerrarSesion();
    this.nombre = "";
    this.estado = "Ofline";
    this.router.navigate(['login']);
    this.nombreMedico = AutentificacionStaticService.buscarUsuarioEnSesion();
  }

}
