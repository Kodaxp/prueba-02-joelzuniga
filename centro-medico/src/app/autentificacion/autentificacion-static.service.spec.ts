import { TestBed, inject } from '@angular/core/testing';

import { AutentificacionStaticService } from './autentificacion-static.service';

describe('AutentificacionStaticService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AutentificacionStaticService]
    });
  });

  it('should be created', inject([AutentificacionStaticService], (service: AutentificacionStaticService) => {
    expect(service).toBeTruthy();
  }));
});
