import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CrearFichaPacienteComponent } from './crear-ficha-paciente/crear-ficha-paciente.component';
import { ListaFichaPacienteComponent } from './lista-ficha-paciente/lista-ficha-paciente.component';
import { EditaFichaPacienteComponent } from './edita-ficha-paciente/edita-ficha-paciente.component';
import { DetalleFichaPacienteComponent } from './detalle-ficha-paciente/detalle-ficha-paciente.component';

const routes: Routes = [
  {path:'ficha-paciente', children:[
    {path:'crear', component: CrearFichaPacienteComponent},
    {path:'listar', component: ListaFichaPacienteComponent},
    {path:'editar/:id', component: EditaFichaPacienteComponent},
    {path:'detalle/:id', component: DetalleFichaPacienteComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FichaPacienteRoutingModule { }
