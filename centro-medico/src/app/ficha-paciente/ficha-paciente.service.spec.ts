import { TestBed, inject } from '@angular/core/testing';

import { FichaPacienteService } from './ficha-paciente.service';

describe('FichaPacienteService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FichaPacienteService]
    });
  });

  it('should be created', inject([FichaPacienteService], (service: FichaPacienteService) => {
    expect(service).toBeTruthy();
  }));
});
