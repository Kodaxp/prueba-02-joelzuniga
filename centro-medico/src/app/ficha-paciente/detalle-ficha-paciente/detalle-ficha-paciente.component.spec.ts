import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleFichaPacienteComponent } from './detalle-ficha-paciente.component';

describe('DetalleFichaPacienteComponent', () => {
  let component: DetalleFichaPacienteComponent;
  let fixture: ComponentFixture<DetalleFichaPacienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleFichaPacienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleFichaPacienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
