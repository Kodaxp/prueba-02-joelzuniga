import { Component, OnInit } from '@angular/core';
import { FichaPacienteService } from '../ficha-paciente.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FichaPaciente } from '../ficha-paciente';

@Component({
  selector: 'app-detalle-ficha-paciente',
  templateUrl: './detalle-ficha-paciente.component.html',
  styleUrls: ['./detalle-ficha-paciente.component.css']
})
export class DetalleFichaPacienteComponent implements OnInit {

  public id:Number;
  public detalleFicha:FichaPaciente;

  constructor(private fichaService:FichaPacienteService,
    private router:Router, private activatedRouter: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRouter.params.forEach((params: Params) => {
      console.log(params['id']);
      this.id = params['id'];
    });

    this.fichaService.listadoFichaPacientePorId(this.id).subscribe(respuesta => {
      this.detalleFicha = respuesta;
    });
  }

  redireccionarEditar(id){
    this.router.navigate(['ficha-paciente/editar',id]);
    console.log(id);
  }
  volverListado(){
    this.router.navigate(['ficha-paciente/listar']);
  }

}
