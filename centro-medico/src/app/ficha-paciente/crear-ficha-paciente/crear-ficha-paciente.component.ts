import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FichaPaciente } from '../ficha-paciente';
import { FichaPacienteService } from '../ficha-paciente.service';

@Component({
  selector: 'app-crear-ficha-paciente',
  templateUrl: './crear-ficha-paciente.component.html',
  styleUrls: ['./crear-ficha-paciente.component.css']
})
export class CrearFichaPacienteComponent implements OnInit {

  public formularioFichaPaciente:FormGroup;

  public nombre:FormControl;
  public apellidoPaterno:FormControl;
  public apellidoMaterno:FormControl;
  public edad:FormControl;
  public correo:FormControl;
  public sistemaSalud:FormControl;
  public sexo:FormControl;
  public salud:String[];
  public sex:String[];

  public ficha:FichaPaciente;

  constructor(private fichaService: FichaPacienteService) { 
    this.nombre = new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z]*'), Validators.maxLength(20)]);
    this.apellidoPaterno = new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z]*'),Validators.maxLength(20)]);
    this.apellidoMaterno = new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z]*'),Validators.maxLength(20)]);
    this.edad = new FormControl('', [Validators.required, Validators.pattern('[0-9]*')]);
    this.correo = new FormControl('', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9._]+\\.[a-z]{2,4}$')]);
    this.sistemaSalud = new FormControl('', Validators.required);
    this.sexo = new FormControl('', Validators.required);
    this.salud = ["Fonasa", "Indigente", "Isapre"];
    this.sex = ["Masculino", "Femenino"];
  }

  ngOnInit() {
    this.formularioFichaPaciente = new FormGroup({
      nombre: this.nombre,
      apellidoPaterno: this.apellidoPaterno,
      apellidoMaterno: this.apellidoMaterno,
      edad: this.edad,
      correo: this.correo,
      sistemaSalud: this.sistemaSalud,
      sexo: this.sexo
    });
  }

  ingresarFichaPaciente(){
    if (this.formularioFichaPaciente.valid) {
      this.ficha = new FichaPaciente;
      this.ficha.nombre = this.nombre.value;
      this.ficha.apellidoPaterno = this.apellidoPaterno.value;
      this.ficha.apellidoMaterno = this.apellidoMaterno.value;
      this.ficha.edad = this.edad.value;
      this.ficha.correo = this.correo.value;
      this.ficha.sexo = this.sexo.value;
      this.ficha.sistemaSalud = this.sistemaSalud.value;
      console.log(this.ficha);
      this.fichaService.guardarFichaPaciente(this.ficha).subscribe(
        (respuesta) => {
          console.log(respuesta);
          alert("Se genero una nueva ficha");
          this.formularioFichaPaciente.reset();
        }
      );
    }
  }

}
