import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearFichaPacienteComponent } from './crear-ficha-paciente.component';

describe('CrearFichaPacienteComponent', () => {
  let component: CrearFichaPacienteComponent;
  let fixture: ComponentFixture<CrearFichaPacienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearFichaPacienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearFichaPacienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
