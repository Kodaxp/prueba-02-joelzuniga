export class FichaPaciente {
    public id:Number;
    public nombre:String;
    public apellidoPaterno:String;
    public apellidoMaterno:String;
    public sexo:String;
    public edad:String;
    public correo:String;
    public sistemaSalud:String;
}
