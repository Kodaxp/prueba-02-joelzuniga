import { Injectable } from '@angular/core';
import { FichaPaciente } from './ficha-paciente';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class FichaPacienteService {

  private URL_API = "http://localhost:3004/fichaPacientes";

  constructor(private httpCliente: HttpClient) { }

  guardarFichaPaciente(ficha:FichaPaciente):Observable<FichaPaciente>{
    return this.httpCliente.post<FichaPaciente>(this.URL_API, ficha);
  }

  listadoFichaPasientes():Observable<FichaPaciente[]>{
    return this.httpCliente.get<FichaPaciente[]>(this.URL_API);
  }

  editarFichaPaciente(ficha:FichaPaciente): Observable<FichaPaciente>{
    return this.httpCliente.put<FichaPaciente>(this.URL_API + '/' + ficha.id, ficha);
  }

  listadoFichaPacientePorId(id):Observable<FichaPaciente>{
    return this.httpCliente.get<FichaPaciente>(this.URL_API + '/' + id);
  }

}
