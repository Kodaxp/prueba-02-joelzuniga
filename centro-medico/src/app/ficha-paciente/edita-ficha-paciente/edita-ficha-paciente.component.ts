import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FichaPaciente } from '../ficha-paciente';
import { FichaPacienteService } from '../ficha-paciente.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-edita-ficha-paciente',
  templateUrl: './edita-ficha-paciente.component.html',
  styleUrls: ['./edita-ficha-paciente.component.css']
})
export class EditaFichaPacienteComponent implements OnInit {

  public formularioFichaPaciente:FormGroup;

  public nombre:FormControl;
  public apellidoPaterno:FormControl;
  public apellidoMaterno:FormControl;
  public edad:FormControl;
  public correo:FormControl;
  public sistemaSalud:FormControl;
  public sexo:FormControl;
  public salud:String[];
  public sex:String[];

  public ficha:FichaPaciente;
  public id:Number;

  constructor(private fichaService:FichaPacienteService,
    private router:Router, private activatedRouter: ActivatedRoute) { 
      this.nombre = new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z]*'), Validators.maxLength(20)]);
      this.apellidoPaterno = new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z]*'),Validators.maxLength(20)]);
      this.apellidoMaterno = new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z]*'),Validators.maxLength(20)]);
      this.edad = new FormControl('', [Validators.required, Validators.pattern('[0-9]*')]);
      this.correo = new FormControl('', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9._]+\\.[a-z]{2,4}$')]);
      this.sistemaSalud = new FormControl('', Validators.required);
      this.sexo = new FormControl('', Validators.required);
      this.salud = ["Fonasa", "Indigente", "Isapre"];
      this.sex = ["Masculino", "Femenino"];
    }

  ngOnInit() {
    this.formularioFichaPaciente = new FormGroup({
      nombre: this.nombre,
      apellidoPaterno: this.apellidoPaterno,
      apellidoMaterno: this.apellidoMaterno,
      edad: this.edad,
      correo: this.correo,
      sistemaSalud: this.sistemaSalud,
      sexo: this.sexo
    });

    this.activatedRouter.params.forEach((params: Params) => {
      console.log(params['id']);
      this.id = params['id'];
    });
    
    this.fichaService.listadoFichaPacientePorId(this.id).subscribe(respuesta => {
      this.ficha = respuesta;
      console.log(this.ficha);

      this.nombre.setValue(this.ficha.nombre);
      this.apellidoPaterno.setValue(this.ficha.apellidoPaterno);
      this.apellidoMaterno.setValue(this.ficha.apellidoMaterno);
      this.edad.setValue(this.ficha.edad);
      this.correo.setValue(this.ficha.correo);
      this.sexo.setValue(this.ficha.sexo);
      this.sistemaSalud.setValue(this.ficha.sistemaSalud);
    });

  }

  editarFichaPaciente(){
    if (this.formularioFichaPaciente.valid) {
      this.ficha.nombre = this.nombre.value;
      this.ficha.apellidoPaterno = this.apellidoPaterno.value;
      this.ficha.apellidoMaterno = this.apellidoMaterno.value;
      this.ficha.edad = this.edad.value;
      this.ficha.correo = this.correo.value;
      this.ficha.sexo = this.sexo.value;
      this.ficha.sistemaSalud = this.sistemaSalud.value;
      console.log(this.ficha);
      this.fichaService.editarFichaPaciente(this.ficha).subscribe(respuesta =>{
        console.log(respuesta);
        this.formularioFichaPaciente.reset();
        this.router.navigate(['ficha-paciente/listar']);
      });
    }
  }
  volverListado(){
    this.router.navigate(['ficha-paciente/listar']);
  }
}
