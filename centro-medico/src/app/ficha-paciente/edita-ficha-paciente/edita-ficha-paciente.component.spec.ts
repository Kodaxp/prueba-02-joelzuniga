import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditaFichaPacienteComponent } from './edita-ficha-paciente.component';

describe('EditaFichaPacienteComponent', () => {
  let component: EditaFichaPacienteComponent;
  let fixture: ComponentFixture<EditaFichaPacienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditaFichaPacienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditaFichaPacienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
