import { Component, OnInit } from '@angular/core';
import { FichaPacienteService } from '../ficha-paciente.service';
import { FichaPaciente } from '../ficha-paciente';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lista-ficha-paciente',
  templateUrl: './lista-ficha-paciente.component.html',
  styleUrls: ['./lista-ficha-paciente.component.css']
})
export class ListaFichaPacienteComponent implements OnInit {
  
  public listadoFichasPacientes: FichaPaciente[];

  constructor(private fichaService: FichaPacienteService, private router:Router) { }

  ngOnInit() {
    this.fichaService.listadoFichaPasientes().subscribe(respuesta =>{
      this.listadoFichasPacientes = respuesta;
    });
  }

  redireccionarEditar(id){
    this.router.navigate(['ficha-paciente/editar',id]);
    console.log(id);
  }

  redireccionarDetalle(id){
    this.router.navigate(['ficha-paciente/detalle',id]);
    console.log(id);
  }

}
