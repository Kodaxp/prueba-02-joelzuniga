import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaFichaPacienteComponent } from './lista-ficha-paciente.component';

describe('ListaFichaPacienteComponent', () => {
  let component: ListaFichaPacienteComponent;
  let fixture: ComponentFixture<ListaFichaPacienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaFichaPacienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaFichaPacienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
