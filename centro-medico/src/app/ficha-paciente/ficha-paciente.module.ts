import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from "@angular/forms";
import { FichaPacienteRoutingModule } from './ficha-paciente-routing.module';
import { FichaPacienteComponent } from './ficha-paciente.component';
import { CrearFichaPacienteComponent } from './crear-ficha-paciente/crear-ficha-paciente.component';
import { ListaFichaPacienteComponent } from './lista-ficha-paciente/lista-ficha-paciente.component';
import { EditaFichaPacienteComponent } from './edita-ficha-paciente/edita-ficha-paciente.component';
import { DetalleFichaPacienteComponent } from './detalle-ficha-paciente/detalle-ficha-paciente.component';
import { FichaPacienteService } from './ficha-paciente.service';

@NgModule({
  imports: [
    CommonModule,
    FichaPacienteRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [FichaPacienteComponent, CrearFichaPacienteComponent, ListaFichaPacienteComponent, EditaFichaPacienteComponent, DetalleFichaPacienteComponent],
  providers: [FichaPacienteService]
})
export class FichaPacienteModule { }
