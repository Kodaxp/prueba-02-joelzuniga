import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CrearFichaAtencionComponent } from './crear-ficha-atencion/crear-ficha-atencion.component';
import { ListarFichaAtencionComponent } from './listar-ficha-atencion/listar-ficha-atencion.component';
import { DetalleFichaAtencionComponent } from './detalle-ficha-atencion/detalle-ficha-atencion.component';

const routes: Routes = [
  {path:'ficha-atencion', children:[
    {path:'crear', component: CrearFichaAtencionComponent},
    {path:'listar', component: ListarFichaAtencionComponent},
    {path:'detalle/:id', component: DetalleFichaAtencionComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FichaAtencionRoutingModule { }
