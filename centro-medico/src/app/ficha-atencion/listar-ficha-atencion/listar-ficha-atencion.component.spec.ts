import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarFichaAtencionComponent } from './listar-ficha-atencion.component';

describe('ListarFichaAtencionComponent', () => {
  let component: ListarFichaAtencionComponent;
  let fixture: ComponentFixture<ListarFichaAtencionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarFichaAtencionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarFichaAtencionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
