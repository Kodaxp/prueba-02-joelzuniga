import { Component, OnInit } from '@angular/core';
import { FichaAtencionService } from '../ficha-atencion.service';
import { FichaAtencion } from '../ficha-atencion';
import { Usuario } from '../../autentificacion/usuario';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listar-ficha-atencion',
  templateUrl: './listar-ficha-atencion.component.html',
  styleUrls: ['./listar-ficha-atencion.component.css']
})
export class ListarFichaAtencionComponent implements OnInit {

  public listadoFichaAtencion:FichaAtencion[];
  public nombreMedico:Usuario;
  public nombre:String;
  public apellido:String;
  public idMedico:Number;

  constructor(private fichaService:FichaAtencionService, private router:Router) { }

  ngOnInit() {
    this.fichaService.listadoFichasAtencion().subscribe(
      (respuesta) => {
        this.listadoFichaAtencion = respuesta;
      }
    );

    this.nombreMedico = FichaAtencionService.buscarMedicoEnSesion();
    this.nombre = this.nombreMedico.nombre;
  }

  redireccionarDetalle(id){
    this.router.navigate(['ficha-atencion/detalle', id]);
    console.log(id);
  }
}
