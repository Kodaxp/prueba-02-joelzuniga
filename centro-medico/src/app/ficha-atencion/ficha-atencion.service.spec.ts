import { TestBed, inject } from '@angular/core/testing';

import { FichaAtencionService } from './ficha-atencion.service';

describe('FichaAtencionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FichaAtencionService]
    });
  });

  it('should be created', inject([FichaAtencionService], (service: FichaAtencionService) => {
    expect(service).toBeTruthy();
  }));
});
