import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearFichaAtencionComponent } from './crear-ficha-atencion.component';

describe('CrearFichaAtencionComponent', () => {
  let component: CrearFichaAtencionComponent;
  let fixture: ComponentFixture<CrearFichaAtencionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearFichaAtencionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearFichaAtencionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
