import { Component, OnInit } from '@angular/core';
import { FichaPaciente } from '../../ficha-paciente/ficha-paciente';
import { FichaAtencionService } from '../ficha-atencion.service';
import { Usuario } from '../../autentificacion/usuario';
import { Patologia } from '../../patologia/patologia';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { FichaAtencion } from '../ficha-atencion';

@Component({
  selector: 'app-crear-ficha-atencion',
  templateUrl: './crear-ficha-atencion.component.html',
  styleUrls: ['./crear-ficha-atencion.component.css']
})
export class CrearFichaAtencionComponent implements OnInit {

  public listadoPacientes:FichaPaciente[];
  public nombreMedico:Usuario;
  public nombre:String;
  public listadoPatologias:Patologia[];

  public paciente:FormControl;
  public patogeno:FormControl;
  public detalle:FormControl;
  public fichaAtencion:FichaAtencion;

  public formularioNuevaAtencion:FormGroup;

  constructor(private fichaService: FichaAtencionService) { 
    this.paciente = new FormControl('', Validators.required);
    this.patogeno = new FormControl('', Validators.required);
    this.detalle = new FormControl('', Validators.required);
  }

  ngOnInit() {
    this.fichaService.listadoFichaPasientes().subscribe(respuesta =>{
      this.listadoPacientes = respuesta;
    });

    this.nombreMedico = FichaAtencionService.buscarMedicoEnSesion();
    this.nombre = this.nombreMedico.nombre;

    this.fichaService.listadoPatologias().subscribe(
      (respuesta) => {
        this.listadoPatologias = respuesta;
      }
    );

    this.formularioNuevaAtencion = new FormGroup({
      paciente: this.paciente,
      patologia: this.patogeno,
      detalle:this.detalle
    });
  }

  ingresarNuevaAtencion(){
    if (this.formularioNuevaAtencion.valid) {
      this.fichaAtencion = new FichaAtencion;
      this.fichaAtencion.nombre = this.nombre;
      this.fichaAtencion.paciente = this.paciente.value;
      this.fichaAtencion.patologia = this.patogeno.value;
      this.fichaAtencion.detalle = this.detalle.value;
      console.log(this.fichaAtencion);
      this.fichaService.guardarFichaPaciente(this.fichaAtencion).subscribe(
        (respuesta) => {
          console.log(respuesta);
        }
      )
      
    }
  }

  agregarAlListado(patologia){
    this.listadoPatologias.push(patologia);
    console.log(this.listadoPatologias);
    
  }

}
