import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FichaAtencionRoutingModule } from './ficha-atencion-routing.module';
import { FichaAtencionComponent } from './ficha-atencion.component';
import { CrearFichaAtencionComponent } from './crear-ficha-atencion/crear-ficha-atencion.component';
import { ListarFichaAtencionComponent } from './listar-ficha-atencion/listar-ficha-atencion.component';
import { EditarFichaAtencionComponent } from './editar-ficha-atencion/editar-ficha-atencion.component';
import { DetalleFichaAtencionComponent } from './detalle-ficha-atencion/detalle-ficha-atencion.component';
import { FichaAtencionService } from './ficha-atencion.service';
import { ReactiveFormsModule } from '@angular/forms';
import { PatologiaComponent } from './patologia/patologia.component';

@NgModule({
  imports: [
    CommonModule,
    FichaAtencionRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [FichaAtencionComponent, CrearFichaAtencionComponent, ListarFichaAtencionComponent, EditarFichaAtencionComponent, DetalleFichaAtencionComponent, PatologiaComponent],
  providers: [FichaAtencionService]
})
export class FichaAtencionModule { }
