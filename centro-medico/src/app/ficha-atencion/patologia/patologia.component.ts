import { Component, OnInit, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PatologiaService } from '../../patologia/patologia.service';
import { Router } from '@angular/router';
import { Patologia } from '../../patologia/patologia';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-patologia',
  templateUrl: './patologia.component.html',
  styleUrls: ['./patologia.component.css']
})
export class PatologiaComponent implements OnInit {

  @Output()
  Patologia: EventEmitter<Patologia> = new EventEmitter<Patologia>();

  public formularioPatologia:FormGroup;

  public nombre:FormControl;
  public patologia:Patologia;

  constructor(private patologiaService: PatologiaService, private router:Router) { 
    this.nombre = new FormControl('', Validators.required);
  }

  ngOnInit() {
    this.formularioPatologia = new FormGroup({
      nombre: this.nombre
    });
  }

  ingresarPatologia(){
    if (this.formularioPatologia.valid) {
      this.patologia = new Patologia();
      this.patologia.nombre = this.nombre.value;
      console.log(this.patologia);
      this.patologiaService.guardarFichaPaciente(this.patologia).subscribe(
        (respuesta) => {
          console.log(respuesta);
          this.formularioPatologia.reset();
          this.router.navigate(['ficha-atencion/crear']);
        }
      );
      this.Patologia.emit(this.patologia);
    }
  }

}
