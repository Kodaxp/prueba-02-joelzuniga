import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FichaAtencionComponent } from './ficha-atencion.component';

describe('FichaAtencionComponent', () => {
  let component: FichaAtencionComponent;
  let fixture: ComponentFixture<FichaAtencionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FichaAtencionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FichaAtencionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
