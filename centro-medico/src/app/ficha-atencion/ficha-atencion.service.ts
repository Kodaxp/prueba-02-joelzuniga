import { Injectable } from '@angular/core';
import { FichaPaciente } from '../ficha-paciente/ficha-paciente';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { Usuario } from '../autentificacion/usuario';
import { Patologia } from '../patologia/patologia';
import { FichaAtencion } from './ficha-atencion';

const NOMBRE_MEDICO = "medico";

@Injectable()
export class FichaAtencionService {

  private URL_API = "http://localhost:3004/fichaPacientes";
  private URL_API_PAT = "http://localhost:3004/patologia";
  private URL_API_ATE = "http://localhost:3004/fichaAtencion";

  constructor(private httpCliente: HttpClient) { }

  listadoFichaPasientes():Observable<FichaPaciente[]>{
    return this.httpCliente.get<FichaPaciente[]>(this.URL_API);
  }

  listadoPatologias():Observable<Patologia[]>{
    return this.httpCliente.get<Patologia[]>(this.URL_API_PAT);
  }

  listadoFichasAtencion():Observable<FichaAtencion[]>{
    return this.httpCliente.get<FichaAtencion[]>(this.URL_API_ATE);
  }
  listadoFichasAtencionPorMedico(id:Number):Observable<FichaAtencion>{
    return this.httpCliente.get<FichaAtencion>(this.URL_API_ATE + + '/' + id);
  }

  static buscarMedicoEnSesion():Usuario{
    if(null == this.buscarMedico(NOMBRE_MEDICO)){
      return null;
    }
    let producto = JSON.parse(this.buscarMedico(NOMBRE_MEDICO));
    return producto;
  }

  static buscarMedico(nombre:string):string{
    return localStorage.getItem(nombre);
  }

  guardarFichaPaciente(ficha:FichaAtencion):Observable<FichaAtencion>{
    return this.httpCliente.post<FichaAtencion>(this.URL_API_ATE, ficha);
  }

}
