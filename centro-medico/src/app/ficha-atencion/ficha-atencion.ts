export class FichaAtencion {
    public id:Number;
    public nombre:String;
    public paciente:String;
    public patologia:String;
    public detalle:String;
    public apellido:String;
}
