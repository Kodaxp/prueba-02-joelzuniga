import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarFichaAtencionComponent } from './editar-ficha-atencion.component';

describe('EditarFichaAtencionComponent', () => {
  let component: EditarFichaAtencionComponent;
  let fixture: ComponentFixture<EditarFichaAtencionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarFichaAtencionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarFichaAtencionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
