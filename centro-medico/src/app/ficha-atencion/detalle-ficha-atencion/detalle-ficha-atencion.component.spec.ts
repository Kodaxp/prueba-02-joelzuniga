import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleFichaAtencionComponent } from './detalle-ficha-atencion.component';

describe('DetalleFichaAtencionComponent', () => {
  let component: DetalleFichaAtencionComponent;
  let fixture: ComponentFixture<DetalleFichaAtencionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleFichaAtencionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleFichaAtencionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
