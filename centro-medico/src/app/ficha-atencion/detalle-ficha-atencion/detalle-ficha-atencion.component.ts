import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FichaAtencionService } from '../ficha-atencion.service';
import { FichaAtencion } from '../ficha-atencion';

@Component({
  selector: 'app-detalle-ficha-atencion',
  templateUrl: './detalle-ficha-atencion.component.html',
  styleUrls: ['./detalle-ficha-atencion.component.css']
})
export class DetalleFichaAtencionComponent implements OnInit {

  public id:Number;
  public detalleFicha:FichaAtencion;

  constructor(private router:Router, private activatedRouter: ActivatedRoute, private fichaService:FichaAtencionService) { }

  ngOnInit() {
    this.activatedRouter.params.forEach((params: Params) => {
      console.log(params['id']);
      this.id = params['id'];
    });

    this.fichaService.listadoFichasAtencionPorMedico(this.id).subscribe(respuesta => {
      this.detalleFicha = respuesta;
    });
  }


  redireccionarListado(){
    this.router.navigate(['ficha-atencion/listar']);
  }
}
