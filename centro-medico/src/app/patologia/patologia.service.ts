import { Injectable } from '@angular/core';
import { Patologia } from './patologia';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class PatologiaService {

  private URL_API = "http://localhost:3004/patologia";

  constructor(private httpClient:HttpClient) { }

  guardarFichaPaciente(patologia:Patologia):Observable<Patologia>{
    return this.httpClient.post<Patologia>(this.URL_API, patologia);
  }

}
