import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CrearPatologiaComponent } from './crear-patologia/crear-patologia.component';

const routes: Routes = [
  {path:'patologia', children:[
    {path:'crear', component:CrearPatologiaComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatologiaRoutingModule { }
