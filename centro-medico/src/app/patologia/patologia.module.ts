import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from "@angular/forms";
import { PatologiaRoutingModule } from './patologia-routing.module';
import { PatologiaComponent } from './patologia.component';
import { CrearPatologiaComponent } from './crear-patologia/crear-patologia.component';
import { PatologiaService } from './patologia.service';

@NgModule({
  imports: [
    CommonModule,
    PatologiaRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [PatologiaComponent, CrearPatologiaComponent],
  providers: [PatologiaService],
  exports:[CrearPatologiaComponent]
})
export class PatologiaModule { }
