import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PatologiaService } from '../patologia.service';
import { Patologia } from '../patologia';
import { Router } from '@angular/router';

@Component({
  selector: 'app-crear-patologia',
  templateUrl: './crear-patologia.component.html',
  styleUrls: ['./crear-patologia.component.css']
})
export class CrearPatologiaComponent implements OnInit {

  public formularioPatologia:FormGroup;

  public nombre:FormControl;
  public patologia:Patologia;

  constructor(private patologiaService: PatologiaService, private router:Router) { 
    this.nombre = new FormControl('', Validators.required);
  }

  ngOnInit() {
    this.formularioPatologia = new FormGroup({
      nombre: this.nombre
    });
  }

  ingresarPatologia(){
    if (this.formularioPatologia.valid) {
      this.patologia = new Patologia();
      this.patologia.nombre = this.nombre.value;
      console.log(this.patologia);
      this.patologiaService.guardarFichaPaciente(this.patologia).subscribe(
        (respuesta) => {
          console.log(respuesta);
          this.formularioPatologia.reset();
          this.router.navigate(['ficha-atencion/crear']);
        }
      );
    }
  }

}
