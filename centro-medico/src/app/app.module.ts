import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { FichaPacienteModule } from './ficha-paciente/ficha-paciente.module';
import { FichaAtencionModule } from './ficha-atencion/ficha-atencion.module';
import { AutentificacionModule } from './autentificacion/autentificacion.module';
import { HttpClientModule } from '@angular/common/http';
import { PatologiaModule } from './patologia/patologia.module';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FichaPacienteModule,
    FichaAtencionModule,
    AutentificacionModule,
    HttpClientModule,
    PatologiaModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
